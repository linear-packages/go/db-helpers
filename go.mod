module gitlab.com/linear-packages/go/db-helpers

go 1.20

require (
	github.com/lib/pq v1.10.9
	gitlab.com/linear-packages/go/secret-manager v0.0.1
)

require (
	github.com/aws/aws-sdk-go v1.42.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
)
