package helper

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	secret "gitlab.com/linear-packages/go/secret-manager"
)

type ConnectionString struct {
	UserName     string `json:"userName"`
	Password     string `json:"password"`
	Host         string `json:"host"`
	Port         string `json:"port"`
	DatabaseName string `json:"databaseName"`
	SearchPath   string `json:"searchPath"`
}

func (cs *ConnectionString) Build(key string) (string, error) {
	sm := secret.NewSecretManager()
	v, err := sm.GetValue(key)
	if err != nil {
		return "", err
	}

	if err = json.Unmarshal([]byte(*v), cs); err != nil {
		return "", err
	}

	return cs.generate(), err
}

func (cs *ConnectionString) generate() string {
	cs.ensureSearchPath()
	s := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s sslmode=disable password=%s search_path=%s",
		cs.Host, cs.Port, cs.UserName, cs.DatabaseName, cs.Password, cs.SearchPath,
	)

	an := os.Getenv("APPLICATION_NAME")
	if strings.TrimSpace(an) != "" {
		return fmt.Sprintf("%s application_name=%s", s, an)
	}
	return s
}

func (cs *ConnectionString) ensureSearchPath() {
	if strings.TrimSpace(cs.SearchPath) == "" {
		cs.SearchPath = "public"
	}
}
