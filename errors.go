package helper

import (
	"database/sql"
	"errors"

	"github.com/lib/pq"
)

func IsDuplicateKeyError(err error) bool {
	const UniqueViolationErr = "23505"
	var perr *pq.Error

	if err == nil {
		return false
	}
	errors.As(err, &perr)
	if perr == nil {
		return false
	}
	return perr.Code == UniqueViolationErr
}

func IsRecordNotFoundError(err error) bool {
	if err == nil {
		return false
	}
	return errors.Is(err, sql.ErrNoRows)
}
