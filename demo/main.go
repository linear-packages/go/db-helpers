package main

import (
	"os"

	helper "gitlab.com/linear-packages/go/db-helpers"
)

func main() {
	p, _ := helper.NewDbProvider("postgresql", os.Getenv("SECRET_DB_CONFIG"), "shared")
	db, _ := p.GetClient()
	defer db.Close()
}
