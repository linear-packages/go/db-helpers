package helper

import (
	"database/sql"
	"errors"
)

type Provider interface {
	GetClient() (*sql.DB, error)
}

type ProviderName string

const (
	Postgresql ProviderName = "postgresql"
	Mysql      ProviderName = "Mysql"
)

func NewDbProvider(provider ProviderName, secretKey, schema string) (Provider, error) {
	switch provider {
	case Postgresql:
		return &PostgreSQL{SecretKey: secretKey}, nil
	case Mysql:
		return nil, errors.New("mysql provider not implemented")
	default:
		return nil, errors.New("provider not supported")
	}
}
