package helper

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type PostgreSQL struct {
	SecretKey string
}

func (p *PostgreSQL) GetClient() (*sql.DB, error) {
	c := ConnectionString{}
	connectionString, err := c.Build(p.SecretKey)
	if err != nil {
		return nil, err
	}

	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}
	createDataBaseSchema(db, c)

	return db, nil
}

func createDataBaseSchema(db *sql.DB, c ConnectionString) {
	var exits bool
	err := db.QueryRow(`SELECT EXISTS (SELECT 1 FROM pg_namespace WHERE nspname = $1)`, c.SearchPath).Scan(&exits)
	if err != nil {
		fmt.Println("Error on verify if schema exist")
		panic(1)
	}

	if exits {
		fmt.Println("Schema Exists.")
		return
	}

	_, err = db.Exec("CREATE SCHEMA " + c.SearchPath)
	if err != nil {
		panic(1)
	}
}
